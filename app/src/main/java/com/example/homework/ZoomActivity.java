package com.example.homework;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class ZoomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);
        getSupportActionBar().setTitle("To do");
        ImageView imageView = findViewById(R.id.img_fade_in);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.zoom);
        imageView.startAnimation(animation);
    }
}